const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.post('/bmi', (req, res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}
    if ( !isNaN(weight) && !isNaN(height) ){
        let bmi = weight / (height * height)
        result = {
            "status" : 200,
            "bmi" : bmi
        }
    }else {

        result = {
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }

    }

    res.send(JSON.stringify(result))
})

app.get('/triangle', (req, res) => {

    let base = parseFloat(req.query.base)
    let height = parseFloat(req.query.height)

    let triangle = 1/2 * base * height
    res.send('Triangle ='+triangle)
})

app.post('/score', (req, res) => {

    let score = parseFloat(req.query.score)
    var graderesult = {}

    if (score >= 80) {
        graderesult = "A"
    }else if (score >= 75) {
        graderesult = "B+" 
    }else if (score >= 70) {
        graderesult = "B"
    }else if (score >= 65) {
        graderesult = "C+"
    }else if (score >= 60) {
        graderesult = "C"
    }else if (score >= 55) {
        graderesult = "D+"
    }else if (score >= 50) {
        graderesult = "D"
    }else if (score < 50) {
        graderesult = "E"
    }else {
        graderesult = {
            "status" : "error"
        }
    }

    res.send(JSON.stringify(req.query.name + "เกรดที่ได้" + graderesult))
})

app.get('/hello', (req, res) => {
    res.send('Sawasdee '+ req.query.name)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})