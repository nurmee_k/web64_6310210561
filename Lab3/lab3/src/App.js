import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Make from './components/Make';


function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <header className='App-header'>
       
       <p>
       <b>นี่คือเว็บของนูรมี</b><br></br>
       </p>
       
      </header>
      <Make />
      <Footer />
    </div>
  );
}

export default App;
