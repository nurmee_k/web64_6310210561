import AboutUs from "../components/AboutUs";

function AboutUsPage() {

    return(
        <div>
            <div align="center">
                <h2> คณะผู้จัดทำ เว็บนี้</h2>

                <AboutUs name="ชินนาม่อน" 
                          address="วัดอังโคะคุจิ"
                          province="กุนมะ" />
                <hr />
                <AboutUs name="นูรมี" 
                          address="อังโคะคุจิ"
                          province="กุนมะ" />

            </div>
        </div>

    );

}

export default AboutUsPage;